# What is this repository for? #

* Aspire QA Automation code challenge

# How do I get set up? #

* Check-out code to local
* Navigate to project folder

   ```
   cd ./{projectName}
   ```

* install 'cypress', 'cypress-xpath' and 'RandExp'

   ```
   npm install
   ```

# TestCase structure #

* ./cypress/integration/aspire/TestCase.js

# How to run test #

* Navigate to project folder

   ```
   cd ./{projectName}
   ```

* Run Cypress TestCase

   ```
   npx cypress run
   ```
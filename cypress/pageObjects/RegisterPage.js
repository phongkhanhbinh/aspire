import { timeStamp } from "console";

const webElementRegisterPageYourPersonalNameTextBox = "//input[@name='full_name']";
const webElementRegisterPageEmailAddressTextBox = "//input[@name='email']";
const webElementRegisterPagePhoneNumberTextBox = "//input[@name='phone']";
const webElementRegisterPagePhoneCountryIcon = "//div[contains(@class, 'flag-select__icon')]";
const webElementRegisterSelectItemCustom = "//div[contains(@class, 'position-engine')]//div[contains(@class, 'item') and text()='%s']";

const webElementRegisterPagePeopleHeardTextBox = "//input[@data-cy='register-person-heard-about']";
const webElementRegisterPageReferralCodeTextBox = "//input[@data-cy='register-person-heard-about-details']";
const webElementRegisterPageIHaveReadAndAgreeCheckBox = "//div[@role='checkbox' and @aria-checked='false']";
const webElementRegisterPageContinueButton = "//button[@type='submit']//span[text()='Continue']";

class RegisterPage {
    inputYourPersonalNameTextBox(name) {
        cy.log('Input Your Personal Name: ' + name);
        cy.xpath(webElementRegisterPageYourPersonalNameTextBox).type(name);
        return this;
    }

    inputEmailAddressTextBox(email) {
        cy.log('Input Email Address: ' + email);
        cy.xpath(webElementRegisterPageEmailAddressTextBox).type(email);
        return this;
    }

    inputPhoneNumber(phoneNumber) {
        cy.log('Input Phone Number: ' + phoneNumber);
        cy.xpath(webElementRegisterPagePhoneNumberTextBox).type(phoneNumber);
        return this;
    }

    clickPhoneCountryIcon() {
        cy.log('Click on Phone Country icon');
        cy.xpath(webElementRegisterPagePhoneCountryIcon).click();
        return this;
    }

    selectPhoneCountryItem(country) {
        const str = country.name + " (" + country.dial_code + ")";
        cy.log('Select phone country item: ' + str);
        const control = webElementRegisterSelectItemCustom.replace('%s', str);
        cy.xpath(control).click();
        return this;
    }

    selectPeopleHeardTextBox(reason, referralCode) {
        cy.log('Select "Where did you hear about us?": ' + reason.name);
        cy.xpath(webElementRegisterPagePeopleHeardTextBox).click();

        const control = webElementRegisterSelectItemCustom.replace('%s', reason.name);
        cy.xpath(control).click();

        return this;
    }

    inputReferralCodeTextBox(referralCode) {
        if (cy.xpath(webElementRegisterPageReferralCodeTextBox).length > 0)
            if (!((!referralCode || /^\s*$/.test(referralCode))))
                cy.xpath(webElementRegisterPageReferralCodeTextBox).type(referralCode);
                
        return this;
    }

    clickIHaveReadAndAgreeCheckBox() {
            cy.xpath(webElementRegisterPageIHaveReadAndAgreeCheckBox).click();
        return this;
    }

    clickContinueButton() {
        cy.xpath(webElementRegisterPageContinueButton).click();
        return this;
    }

}

export default RegisterPage;
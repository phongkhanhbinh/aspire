Cypress.config();

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from failing the test
    return false
})

class TestCaseBase {
    // FIXTURES_PATH = '../../fixtures';
    visit() {
        cy.visit("/sg");
    }
}

export default TestCaseBase;
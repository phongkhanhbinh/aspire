const webElementVerifyingPageButton = "//button[@type='submit']//span[@class='block']";

class VerifyingPage {

    clickOnButton() {
        cy.log('Click on button');
        cy.xpath(webElementVerifyingPageButton, {timeout: 60000}).click();
        return this;
    }
}

export default VerifyingPage;
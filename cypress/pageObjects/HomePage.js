const webElementHomePageRegisterLink = "//a[contains(text(), 'Register')]";

class HomePage {

    clickRegisterLink() {
        cy.xpath(webElementHomePageRegisterLink).click();
        return this;
    }
}

export default HomePage;
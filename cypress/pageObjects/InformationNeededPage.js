const webElementInformationNeededPagePersonDetailsStatus = "//div[@aria-label='%name' and @aria-checked='%status']";
const webElementInformationNeededPageGetStartedButton = "//button[@type='submit']//span[text()='Get Started']";
const webElementInformationNeededPageContinueButton = "//button[@type='submit']//span[text()='Continue']";

class InformationNeededPage {
    validatePersonStatus(name, status) {
        cy.log('Validate Person Details status: ' + name + " - " + status);
        let control = webElementInformationNeededPagePersonDetailsStatus.replace('%name', name).replace('%status', status);
        cy.xpath(control).should('be.visible');
        return this;
    }

    clickGetStartedButton() {
        cy.xpath(webElementInformationNeededPageGetStartedButton).click();
        return this;
    }

    clickContinueButton() {
        cy.xpath(webElementInformationNeededPageContinueButton).click();
        return this;
    }
}

export default InformationNeededPage;
const webElementPersonalDetailsPageYourPersonalNameLabel = "//div[@label='Your personal name']//div[@class='q-mt-md']";
const webElementPersonalDetailsPagePhoneNumberLabel = "//div[@label='Phone number']//div[@class='q-mt-md']";
const webElementPersonalDetailsPageEmailAddressLabel = "//div[@label='Email address']//input";

const webElementPersonalDetailsPageDateOfBirthTextBox = "//div[@label='Date of Birth']//input";
const webElementPersonalDetailsPageCalendarDialogMonthButton = "//div[contains(@class, 'q-date')]//div[contains(@class, 'q-date__navigation')]//div[contains(@class, 'relative-position')][1]//button";
const webElementPersonalDetailsPageCalendarDialogYearButton = "//div[contains(@class, 'q-date')]//div[contains(@class, 'q-date__navigation')]//div[contains(@class, 'relative-position')][2]//button";
const webElementPersonalDetailsPageCalendarDialogMonthItemCustom = "//div[contains(@class, 'q-date')]//div[contains(@class, 'q-date__months-item')]//span[text()='%s']";
const webElementPersonalDetailsPageCalendarDialogYearItemCustom = "//div[contains(@class, 'q-date')]//div[contains(@class, 'q-date__years-item')]//span[text()='%s']";
const webElementPersonalDetailsPageCalendarDialogDateItemCustom = "//div[contains(@class, 'q-date')]//div[contains(@class, 'q-date__calendar-days')]//span[text()='%s']";

const webElementPersonalDetailsPageNationalityTextBox = "//div[@label='Nationality']//input";
const webElementPersonalDetailsPageNationalityItemCustom = "//div[contains(@class, 'q-menu')]//div[text()='%s']";

const webElementPersonalDetailsPageGenderTextBox = "//div[@label='Gender']//input";
const webElementPersonalDetailsPageGenderItemCustom = "//div[contains(@class, 'q-menu')]//div[text()='%s']";

const webElementPersonalDetailsPageWhatAreYouLookingForTextBox = "//div[@label='What are you looking for?']//input";
const webElementPersonalDetailsPageWhatAreYouLookingForItemCustom = "//div[contains(@class, 'q-menu')]//div[text()='%s']";

const webElementPersonalDetailsPageSubmitButton = "//button[@type='submit']//span[text()='Submit']";

class PersonalDetailsPage {
    validateYourPersonalName(name) {
        cy.log('validate Your Personal Name: ' + name);
        cy.xpath(webElementPersonalDetailsPageYourPersonalNameLabel).should('contain', name);
        return this;
    }

    validatePhoneNumber(phoneNumber) {
        cy.log('validate Phone Number: ' + phoneNumber);
        cy.xpath(webElementPersonalDetailsPagePhoneNumberLabel).should('contain', phoneNumber);
        return this;
    }

    validateEmailAddress(emailAddress) {
        cy.log('validate Email Address: ' + emailAddress);
        cy.xpath(webElementPersonalDetailsPageEmailAddressLabel).should('have.value', emailAddress);
        return this;
    }

    selectDateOfBirth(date, month, year) {
        cy.log('Select Date Of Birth: ' + month + ' ' + date + ", " + year);
        cy.xpath(webElementPersonalDetailsPageDateOfBirthTextBox).click();
        cy.xpath(webElementPersonalDetailsPageCalendarDialogMonthButton).click();
        const monthControl = webElementPersonalDetailsPageCalendarDialogMonthItemCustom.replace('%s', month);
        cy.xpath(monthControl).click();

        cy.xpath(webElementPersonalDetailsPageCalendarDialogYearButton).click();
        const yearControl = webElementPersonalDetailsPageCalendarDialogYearItemCustom.replace('%s', year);
        cy.xpath(yearControl).click();

        const dateControl = webElementPersonalDetailsPageCalendarDialogDateItemCustom.replace('%s', date);
        cy.xpath(dateControl).click();

        return this;
    }

    selectNationality(nationality) {
        cy.log('Select Nationality: ' + nationality.name);
        cy.xpath(webElementPersonalDetailsPageNationalityTextBox).click();
        cy.xpath(webElementPersonalDetailsPageNationalityTextBox).type(nationality.name);
        const control = webElementPersonalDetailsPageNationalityItemCustom.replace('%s', nationality.name);
        cy.log('Control: ' + control)
        cy.xpath(control).click();
        return this;
    }

    selectGender(gender) {
        cy.log('Select Gender: ' + gender);
        cy.xpath(webElementPersonalDetailsPageGenderTextBox).click();
        const control = webElementPersonalDetailsPageGenderItemCustom.replace('%s', gender);
        cy.xpath(control).click();
        return this;
    }

    selectWhatAreYouLookingFor(whatAreYouLookingFor) {
        cy.log('Select What Are You Looking For: ' + whatAreYouLookingFor.name);
        cy.xpath(webElementPersonalDetailsPageWhatAreYouLookingForTextBox).click();
        const control = webElementPersonalDetailsPageWhatAreYouLookingForItemCustom.replace('%s', whatAreYouLookingFor.name);
        cy.xpath(control).click();
        return this;
    }

    clickSubmitButton() {
        cy.xpath(webElementPersonalDetailsPageSubmitButton).click();
        return this;
    }
}

export default PersonalDetailsPage;
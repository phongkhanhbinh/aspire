const webElementIncorporateSelectorPageYesMyBusinessContinueButton = "//div[contains(@class, 'column-content') and descendant::span[contains(., 'Yes')]]//button//span[text()='Continue']";
const webElementIncorporateSelectorPageIDontHaveContinueButton = "//div[contains(@class, 'column-content') and descendant::span[contains(., 'I don')]]//button//span[text()='Continue']";
const webElementIncorporateSelectorPageIncorporateYourCompanyLabel = "//div[contains(text(), 'Incorporate your company and open a business account on the same day')]";

class IncorporateSelectorPage {
    clickOnYesMyBusinessIsRegisteredInSingaporeWithAcra() {
        cy.log('Click on Continue button of "Yes, my business is registered in Singapore with ACRA" secion');
        cy.xpath(webElementIncorporateSelectorPageYesMyBusinessContinueButton).click();
        return this;
    }

    clickOnIDontHaveABusinessYetContinueButton() {
        cy.log('Click on Continue button of "I don’t have a business yet" secion');
        cy.xpath(webElementIncorporateSelectorPageIDontHaveContinueButton).click();
        return this;
    }

    validateIncorporateYourCompanyMessageExists() {
        cy.log('Validate "Incorporate your company and open a business account on the same day" message exists');
        cy.xpath(webElementIncorporateSelectorPageIncorporateYourCompanyLabel).should('be.visible');
        return this;
    }
}

export default IncorporateSelectorPage;
const webElementOtpPageActivateCodeTextBox = "//input[@data-cy='digit-input-pin']";
const webElementOtpPageVerifyOtpNameLabel = "//div[contains(@class, 'verify-otp__message-name') and contains(., '')]";
const webElementOtpPageVerifyOtpPhoneLabel = "//div[contains(@class, 'verify-otp__message-recipient') and contains(., '')]";
const webElementOtpPageVerifyOtpEmailLabel = "//div[contains(@class, 'verify-otp__message-recipient') and contains(., '')]";
const webElementOtpPageVerifyOtpVerifyButton = "//button[@type='submit']//span[text()='Verify']";
const webElementOtpPageVerifyOtpContinueButton = "//button[@type='submit']//span[text()='Continue']";

class OtpPage {

    inputOtpCode(code) {
        cy.log('Input OTP code: ' + code);
        cy.xpath(webElementOtpPageActivateCodeTextBox).type(code);
        return this;
    }

    validateName(name) {
        cy.log('Validate name: ' + name);
        const control = webElementOtpPageVerifyOtpNameLabel.replace('%s', name);
        cy.xpath(control).should('be.visible');
        return this;
    }

    waitVerifyCode() {
        cy.wait(5000);
    }

    validatePhone(phone) {
        cy.log('Validate phone: ' + phone);
        const control = webElementOtpPageVerifyOtpPhoneLabel.replace('%s', phone);
        cy.xpath(control).should('be.visible');
        return this;
    }

    validateEmail(email) {
        cy.log('Validate email: ' + email);
        const control = webElementOtpPageVerifyOtpEmailLabel.replace('%s', email);
        cy.xpath(control).should('be.visible');
        return this;
    }

    clickVerifyButton() {
        cy.log('Click Verify button');
        cy.xpath(webElementOtpPageVerifyOtpVerifyButton).click();
        return this;
    }

    clickContinueButton() {
        cy.log('Click Continue button');
        cy.xpath(webElementOtpPageVerifyOtpContinueButton).click();
        return this;
    }
}

export default OtpPage;
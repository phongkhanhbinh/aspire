import CommonUtil from '../commons/CommonUtil';

const commonUtil = new CommonUtil();

const webElementBusinessDetailsPageBusinessNameInput = "//div[@label='Business Name']//input";
const webElementBusinessDetailsPageRegistrationTypeInput = "//div[@label='Registration Type']//input";
const webElementBusinessDetailsPageBusinessRegistrationNumberInput = "//div[@label='Business Registration Number (UEN)']//input";
const webElementBusinessDetailsPageWhatIsYourRoleInput = "//div[@label='What is your role?']//input";
const webElementBusinessDetailsPageCountryInput = "//div[@label='Country']//input";
const webElementBusinessDetailsPageIndustryInput = "//div[@label='Industry']//input";
const webElementBusinessDetailsPageSubIndustryInput = "//div[@label='Sub Industry']//input";
const webElementBusinessDetailsPagePopupItemCustom = "//div[contains(@class, 'q-menu')]//div[text()='%s']";
const webElementBusinessDetailsPageSelectIcon = "//i[contains(@class, 'select')]";

const webElementBusinessDetailsPageSubmitButton = "//button[@type='submit']//span[text()='Submit']";

class BusinessDetailsPage {
    
    typeBusinessName(businessName) {
        cy.log('Enter business name: ' + businessName);
        cy.xpath(webElementBusinessDetailsPageBusinessNameInput).type(businessName);
        return this;
    }
    
    selectRegistrationType(registrationType) {
        cy.log('Select Registration Type: ' + registrationType.name);
        cy.xpath(webElementBusinessDetailsPageRegistrationTypeInput).click();
        cy.xpath(webElementBusinessDetailsPageRegistrationTypeInput).type(registrationType.name.substr(0, 3));
        this.clickPopupItem(registrationType.name);
        return this;
    }
    
    typeBusinessRegistrationNumber(uen) {
        cy.log('Enter Business Registration Number (UEN): ' + uen);
        cy.xpath(webElementBusinessDetailsPageBusinessRegistrationNumberInput).type(uen);
        return this;
    }
    
    selectWhatIsYourRole(role) {
        cy.log('Select What Is Your Role: ' + role.name);
        cy.xpath(webElementBusinessDetailsPageWhatIsYourRoleInput).click();
        cy.xpath(webElementBusinessDetailsPageWhatIsYourRoleInput).type(role.name.substr(0, 3));
        this.clickPopupItem(role.name);
        return this;
    }
    
    selectIndustry(industry) {
        cy.log('Select Industry: ' + industry.name);
        cy.xpath(webElementBusinessDetailsPageIndustryInput).click();
        cy.xpath(webElementBusinessDetailsPageIndustryInput).type(industry.name.substr(0,3));
        this.clickPopupItem(industry.name);
        return this;
    }
    
    selectSubIndustry(subIndustry) {
        cy.log('Select Sub Industry: ' + subIndustry.name);
        cy.xpath(webElementBusinessDetailsPageSubIndustryInput).click();
        cy.xpath(webElementBusinessDetailsPageSubIndustryInput).type(subIndustry.name.substr(0,3));
        this.clickPopupItem(subIndustry.name);
        return this;
    }

    clickPopupItem(item) {
        const control = webElementBusinessDetailsPagePopupItemCustom.replace('%s', item);
        cy.log('Click on item of drop-down');
        cy.xpath(control).click();
        return this;
    }

    clickSubmitButton() {
        cy.xpath(webElementBusinessDetailsPageSubmitButton).click();
        return this;
    }

    waitSelectDropDownIconExists() {
        cy.xpath(webElementBusinessDetailsPageSelectIcon).should('exist');
        return this;
    }

    waitForPageNotExist() {
        cy.xpath(webElementBusinessDetailsPageBusinessNameInput, {timeout: 60000}).should('not.exist');
        return this;
    }
}

export default BusinessDetailsPage;
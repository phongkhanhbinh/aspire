class JsonUtil {
    randomItemFromJsonObject(jsonList) {
        let jsonArray = [];
        for (let i in jsonList) {
            jsonArray.push(jsonList[i]);
        }
        return jsonArray[Math.floor(Math.random() * jsonArray.length)];
    }
}
export default JsonUtil;
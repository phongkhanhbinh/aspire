let BUSINESS_REGISTRATION_NUMBER_UEN = '^([0-9]{8,9}[a-zA-Z]{1})$';
const DEFAULT_WAITING_TIME = 10;
const LONG_WAITING_TIME = 30;

class CommonUtils {
    randomStringWithRegex(len, regex) {
        regex = regex || BUSINESS_REGISTRATION_NUMBER_UEN;
        const RandExp = require('randexp');
        return new RandExp(regex).gen();
    }

    randomString(len, charSet) {
        charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let randomString = '';
        for (var i = 0; i < len; i++) {
            let randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return randomString;
    }

    randomStringWithSpace(len, space) {
        let charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let randomString = '';
        let s = 0;
        for (var i = 0; i < len; i++) {
            let randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
            if (s >= space) {
                randomString += " ";
                i++;
                s = 0;
            } else {
                s++;
            }
        }
        return randomString;
    }

    randomEmail(len) {
        let charSet = 'abcdefghijklmnopqrstuvwxyz';
        return this.randomString(len, charSet) + '@' + this.randomString(5, charSet) + "." + this.randomString(3, charSet);
    }

    randomNumber(len) {
        let charSet = '0123456789';
        return this.randomString(len, charSet);
    }

    upperCaseFirstLetterOfEachWord(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }
}

export default CommonUtils;

import TestCaseBase from '../../pageObjects/TestCaseBase';
import HomePage from '../../pageObjects/HomePage';
import RegisterPage from '../../pageObjects/RegisterPage';
import OtpPage from '../../pageObjects/OtpPage';
import IncorporateSelectorPage from '../../pageObjects/IncorporateSelectorPage';
import InformationNeededPage from '../../pageObjects/InformationNeededPage';
import PersonalDetailsPage from '../../pageObjects/PersonalDetailsPage';
import BusinessDetailsPage from '../../pageObjects/BusinessDetailsPage';
import VerifyingPage from '../../pageObjects/VerifyingPage';
import CommonUtil from '../../commons/CommonUtil';
import JsonUtil from '../../commons/JsonUtil';

const tcBase = new TestCaseBase();
const homePage = new HomePage();
const registerPage = new RegisterPage();
const otpPage = new OtpPage();
const incorporateSelectorPage = new IncorporateSelectorPage();
const informationNeededPage = new InformationNeededPage();
const personalDetailsPage = new PersonalDetailsPage();
const businessDetailsPage = new BusinessDetailsPage();
const verifyingPage = new VerifyingPage();
const commonUtil = new CommonUtil();
const jsonUtil = new JsonUtil();

describe('TC02 - User can complete the Registration flow wihtout business in Singapore', () => {

    // let phoneCountryList = require(tcBase.getFilePathInFixturesFolder('register/PhoneCountryList'));
    let phoneItem = jsonUtil.randomItemFromJsonObject(require('../../fixtures/PhoneCountryList'));
    let whereYouHear = jsonUtil.randomItemFromJsonObject(require('../../fixtures/WhereDidYouHearAboutUs'));

    let personalName = 'Person ' + commonUtil.randomStringWithSpace(25, 6);
    let emailAddress = commonUtil.randomEmail(6);
    let phoneNumber = commonUtil.randomNumber(8);
    let referralCode = commonUtil.randomString(10);

    it('Load page', () => {
        tcBase.visit();
    });

    it('Click on Register link on Home page', () => {
        homePage.clickRegisterLink();
    });

    it('Register a new account', () => {
        registerPage.inputYourPersonalNameTextBox(personalName)
            .inputEmailAddressTextBox(emailAddress)
            .clickPhoneCountryIcon()
            .selectPhoneCountryItem(phoneItem)
            .inputPhoneNumber(phoneNumber)
            .selectPeopleHeardTextBox(whereYouHear, referralCode)
            .clickIHaveReadAndAgreeCheckBox()
            .clickContinueButton();

    });

    it('Input OTP', () => {
        otpPage.validateName(personalName);
        
        otpPage.validatePhone(phoneItem.dial_code + " " + phoneNumber);

        otpPage.waitVerifyCode();

        otpPage.inputOtpCode('123456 {enter}');

        otpPage.clickContinueButton();
    });

    it('Select "I dont have business yet"', () => {
        incorporateSelectorPage.clickOnIDontHaveABusinessYetContinueButton();
        incorporateSelectorPage.validateIncorporateYourCompanyMessageExists();
    });
});

import TestCaseBase from '../../pageObjects/TestCaseBase';
import HomePage from '../../pageObjects/HomePage';
import RegisterPage from '../../pageObjects/RegisterPage';
import OtpPage from '../../pageObjects/OtpPage';
import IncorporateSelectorPage from '../../pageObjects/IncorporateSelectorPage';
import InformationNeededPage from '../../pageObjects/InformationNeededPage';
import PersonalDetailsPage from '../../pageObjects/PersonalDetailsPage';
import BusinessDetailsPage from '../../pageObjects/BusinessDetailsPage';
import VerifyingPage from '../../pageObjects/VerifyingPage';
import CommonUtil from '../../commons/CommonUtil';
import JsonUtil from '../../commons/JsonUtil';

const tcBase = new TestCaseBase();
const homePage = new HomePage();
const registerPage = new RegisterPage();
const otpPage = new OtpPage();
const incorporateSelectorPage = new IncorporateSelectorPage();
const informationNeededPage = new InformationNeededPage();
const personalDetailsPage = new PersonalDetailsPage();
const businessDetailsPage = new BusinessDetailsPage();
const verifyingPage = new VerifyingPage();
const commonUtil = new CommonUtil();
const jsonUtil = new JsonUtil();

describe('TC01 - User can complete the Registration flow with business in Singapore', () => {

    // let phoneCountryList = require(tcBase.getFilePathInFixturesFolder('register/PhoneCountryList'));
    let phoneItem = jsonUtil.randomItemFromJsonObject(require('../../fixtures/PhoneCountryList'));
    let whereYouHear = jsonUtil.randomItemFromJsonObject(require('../../fixtures/WhereDidYouHearAboutUs'));
    let whatAreYouLookingFor = jsonUtil.randomItemFromJsonObject(require('../../fixtures/WhatAreYouLookingFor'));

    let industry = jsonUtil.randomItemFromJsonObject(require('../../fixtures/Business/Industry'));
    let subIndustry = jsonUtil.randomItemFromJsonObject(industry.subIndustry);
    let registrationType = jsonUtil.randomItemFromJsonObject(require('../../fixtures/Business/RegistrationType'));
    let whatIsYourRole = jsonUtil.randomItemFromJsonObject(require('../../fixtures/Business/WhatIsYourRole'));

    let personalName = 'Person ' + commonUtil.randomStringWithSpace(25, 6);
    let emailAddress = commonUtil.randomEmail(6);
    let phoneNumber = commonUtil.randomNumber(8);
    let referralCode = commonUtil.randomString(10);
    let businessName = 'Business ' + commonUtil.randomStringWithSpace(25, 6);

    it('Load page', () => {
        tcBase.visit();
    });

    it('Click on Register link on Home page', () => {
        homePage.clickRegisterLink();
    });

    it('Register a new account', () => {
        registerPage.inputYourPersonalNameTextBox(personalName)
            .inputEmailAddressTextBox(emailAddress)
            .clickPhoneCountryIcon()
            .selectPhoneCountryItem(phoneItem)
            .inputPhoneNumber(phoneNumber)
            .selectPeopleHeardTextBox(whereYouHear, referralCode)
            .clickIHaveReadAndAgreeCheckBox()
            .clickContinueButton();

    });

    it('Input OTP', () => {
        otpPage.validateName(personalName);
        
        otpPage.validatePhone(phoneItem.dial_code + " " + phoneNumber);

        otpPage.waitVerifyCode();

        otpPage.inputOtpCode('123456 {enter}');

        otpPage.clickContinueButton();
    });

    it('Select "Yes, my business is registered in Singapore with ACRA"', () => {
        incorporateSelectorPage.clickOnYesMyBusinessIsRegisteredInSingaporeWithAcra();
    });

    it('Validate Person details', () => {
        informationNeededPage.validatePersonStatus("Full name", true);
        informationNeededPage.validatePersonStatus("Mobile phone", true);
        informationNeededPage.validatePersonStatus("Email address", true);
        informationNeededPage.validatePersonStatus("Date of birth", false);
        informationNeededPage.validatePersonStatus("Gender", false);
        informationNeededPage.validatePersonStatus("Nationality", false);
        informationNeededPage.clickGetStartedButton();
    });

    it('Full fill "Personal Details" page', () => {
        personalDetailsPage.validateYourPersonalName(commonUtil.upperCaseFirstLetterOfEachWord(personalName));
        personalDetailsPage.validatePhoneNumber(phoneItem.dial_code + " " + phoneNumber);
        personalDetailsPage.validateEmailAddress(emailAddress);
        personalDetailsPage.selectDateOfBirth(1, 'Jan', 2000);
        personalDetailsPage.selectNationality(phoneItem);
        personalDetailsPage.selectGender('Male');
        personalDetailsPage.selectWhatAreYouLookingFor(whatAreYouLookingFor);
        personalDetailsPage.clickSubmitButton();
    });

    it('Input OTP', () => {
        otpPage.validateName(commonUtil.upperCaseFirstLetterOfEachWord(personalName));
        
        otpPage.validatePhone(emailAddress);

        otpPage.waitVerifyCode();

        otpPage.inputOtpCode('123456 {enter}');

        otpPage.clickContinueButton();
    });

    it('Validate Business details', () => {
        informationNeededPage.validatePersonStatus("Business Name", false);
        informationNeededPage.validatePersonStatus("Registration Type", false);
        informationNeededPage.validatePersonStatus("Business Registration Number (UEN)", false);
        informationNeededPage.validatePersonStatus("What is your role?", false);
        informationNeededPage.validatePersonStatus("Country", false);
        informationNeededPage.validatePersonStatus("Industry", false);
        informationNeededPage.validatePersonStatus("Sub Industry", false);
        informationNeededPage.clickContinueButton();
    });

    it('Full fill Business details information', () => {
        businessDetailsPage.waitSelectDropDownIconExists();
        businessDetailsPage.typeBusinessName(businessName);
        businessDetailsPage.selectRegistrationType(registrationType);
        businessDetailsPage.typeBusinessRegistrationNumber(commonUtil.randomStringWithRegex());
        businessDetailsPage.selectWhatIsYourRole(whatIsYourRole);
        businessDetailsPage.selectIndustry(industry);
        businessDetailsPage.selectSubIndustry(subIndustry);
        businessDetailsPage.clickSubmitButton();
        businessDetailsPage.waitForPageNotExist();
    });

    it('Click button on Verifying page', () => {
        verifyingPage.clickOnButton();
    });
});